//
//  ErrorHandle.swift
//  UnderArmourConnectedFitness
//
//  Created by Cesar Brenes on 5/2/17.
//  Copyright © 2017 Veux Labs. All rights reserved.
//

import Foundation

class ErrorHandle {

    private class func getDictionaryForSpecificError(errorCode: Int) -> [String: Any]{
        let errorsDictionary = getErrorArrayFromPlist()
        let result = errorsDictionary?.filter{($0["codeKey"] as! NSNumber).intValue == errorCode}
        if result?.count == 0{
            return  getDictionaryForSpecificError(errorCode:1015)
        }
        else{
            return result!.first!
        }
    }
    
    
    private class func getErrorArrayFromPlist() -> [[String: Any]]?{
        let underArmourBundle = Constants.bundle
        if let fileUrl = underArmourBundle.url(forResource: "errorCodes", withExtension: "plist"),
            let data = try? Data(contentsOf: fileUrl) {
            if let result = try? PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [[String: Any]] {
                return result
            }
        }
        return nil
    }
    
    class func createErrorCode(errorCode: Int) -> NSError{
        let errorDictionary = getDictionaryForSpecificError(errorCode: errorCode)
        let userInfo: [AnyHashable : Any] = [
                NSLocalizedDescriptionKey :  NSLocalizedString("Error", value: errorDictionary[Constants.errorDescriptionKey] as! String, comment: "") ,
                NSLocalizedFailureReasonErrorKey : NSLocalizedString("Error", value: errorDictionary[Constants.errorFailureReasonKey] as! String, comment: ""),
                NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString("Error", value: errorDictionary[Constants.errorSuggestionKey] as! String, comment: "")
        ]
        return NSError(domain: "UnderArmourResponseError", code: errorCode, userInfo: userInfo)
    }

    
}
