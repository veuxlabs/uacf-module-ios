//
//  UACFWorkoutValidator.swift
//  UnderArmourConnectedFitness
//
//  Created by Cesar Brenes on 5/4/17.
//  Copyright © 2017 Veux Labs. All rights reserved.
//

import Foundation


class UACFWorkoutValidator{
    

    class func validateAllUnitsInTheWorkoutDictionary(workout: [String: Any]) -> Bool{
        if !validateIfValueHasUnits(workout: workout, valueKey: WorkoutKeys.DISTANCE_UNIT, unitKey: WorkoutKeys.DISTANCE_UNIT){
            print("Distance doesn't have it's unit")
            return false
        }
        if !validateIfValueHasUnits(workout: workout, valueKey: WorkoutKeys.SPEED_AVG, unitKey: WorkoutKeys.SPEED_UNIT){
            print("Speed AVG doesn't have it's unit")
            return false
        }
        if !validateIfValueHasUnits(workout: workout, valueKey: WorkoutKeys.ELAPSED_TIME, unitKey: WorkoutKeys.ELAPSED_TIME_UNIT){
            print("Elapsed time doesn't have it's unit")
            return false
        }
        if !validateIfValueHasUnits(workout: workout, valueKey: WorkoutKeys.METABOLIC_ENERGY_TOTAL, unitKey: WorkoutKeys.METABOLIC_ENERGY_TOTAL_UNIT){
            print("Metabolic energy total doesn't have it's unit")
            return false
        }
        return true
    }
    
    
    class func validateIfValueHasUnits(workout: [String: Any], valueKey: String, unitKey:String) -> Bool{
        var valueExists = false
        var keyExists = false
        if let _ = workout[valueKey]{
            valueExists = true
        }
        if let _ = workout[unitKey]{
            keyExists = true
        }
        if (valueExists && keyExists) || (!valueExists && !keyExists){
            return true
        }
        return false
    }

    
    public class func validateDataTypes(workout: [String: Any]) -> Bool{
        if !validateAllStringsTypes(workout: workout){
            return false
        }
        if !validateAllIntsTypes(workout: workout){
            return false
        }
        if !validateAllUnitTypes(workout: workout){
            return false
        }
        if !validateAllDoubleTypes(workout: workout){
            return false
        }
        if !validateType(workout: workout, isMandatory: true, workoutKey: WorkoutKeys.START_TIME, type: Date.self){
            print("Start Time has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: true, workoutKey: WorkoutKeys.TIME_ZONE, type: TimeZone.self){
            print("Time Zone has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: true, workoutKey: WorkoutKeys.ACTIVITY_TYPE, type: WorkoutActivityTypes.self){
            print("Activity type has wrong type")
            return false
        }
        return true
    }
    
    
    private class func validateAllStringsTypes(workout: [String: Any]) -> Bool{
        if !validateType(workout: workout, isMandatory: true, workoutKey: WorkoutKeys.NAME, type: String.self){
            print("Name has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: true, workoutKey: WorkoutKeys.CONSOLE_USER_NUMBER, type: String.self){
            print("Console user number has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: true, workoutKey: WorkoutKeys.RECORD_ID, type: String.self){
            print("Record Id has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: true, workoutKey: WorkoutKeys.MACHINE_TYPE, type: String.self){
            print("Machine Type has wrong type")
            return false
        }
        return true
    }
    
    private class func validateAllIntsTypes(workout: [String: Any]) -> Bool{
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.METABOLIC_ENERGY_TOTAL, type: Int.self){
            print("Metabolic energy total has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.ELAPSED_TIME, type: Int.self){
            print("Elapsed time has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.HEART_RATE_AVG, type: Int.self){
            print("Heart Rate AVG has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.POWER_AVG, type: Int.self){
            print("Power AVG has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.DAY, type: Int.self){
            print("Day has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.MONTH, type: Int.self){
            print("Month has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.YEAR, type: Int.self){
            print("Year has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.SECONDS, type: Int.self){
            print("Seconds has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.MINUTES, type: Int.self){
            print("Minutes has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.HOUR, type: Int.self){
            print("Hour has wrong type")
            return false
        }
        return true
    }
    
    private class func validateAllUnitTypes(workout: [String: Any]) -> Bool{
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.DISTANCE_UNIT, type: WorkoutDistanceUnit.self){
            print("Distance has wrong unit")
            return false
        }
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.METABOLIC_ENERGY_TOTAL_UNIT, type: WorkoutMetabolicEnergy.self){
            print("Metabolic energy total has wrong unit")
            return false
        }
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.ELAPSED_TIME_UNIT, type: WorkoutElapsedTime.self){
            print("Elapsed time has wrong unit")
            return false
        }
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.SPEED_UNIT, type: WorkoutSpeedUnit.self){
            print("Speed has wrong unit")
            return false
        }
        return true
    }
    
    
    private class func validateAllDoubleTypes(workout: [String: Any]) -> Bool{
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.SPEED_AVG, type: Double.self){
            print("Speed AVG has wrong type")
            return false
        }
        if !validateType(workout: workout, isMandatory: false, workoutKey: WorkoutKeys.DISTANCE, type: Double.self){
            print("Distance has wrong type")
            return false
        }
        return true
    }
    
    
    private class func validateType<T>(workout: [String: Any], isMandatory: Bool, workoutKey: String, type: T.Type) -> Bool{
        let isCorrectType = workout[workoutKey] is T
        guard let _ = workout[workoutKey] else {
            if isMandatory{
                return false
            }
            return true
        }
        return isCorrectType
    }
    
    
    class func hasTheWorkoutTheMandatoryFields(workout: [String: Any]) -> Bool{
        guard let _ = workout[WorkoutKeys.NAME] else {
            return false
        }
        guard let _ = workout[WorkoutKeys.START_TIME] else {
            return false
        }
        guard let _ = workout[WorkoutKeys.TIME_ZONE] else {
            return false
        }
        guard let _ = workout[WorkoutKeys.ACTIVITY_TYPE] else {
            return false
        }
        guard let _ = workout[WorkoutKeys.CONSOLE_USER_NUMBER] else {
            return false
        }
        guard let _ = workout[WorkoutKeys.RECORD_ID] else {
            return false
        }
        guard let _ = workout[WorkoutKeys.MACHINE_TYPE] else {
            return false
        }
        guard let _ = workout[WorkoutKeys.DAY] else {
            return false
        }
        guard let _ = workout[WorkoutKeys.MONTH] else {
            return false
        }
        guard let _ = workout[WorkoutKeys.YEAR] else {
            return false
        }
        guard let _ = workout[WorkoutKeys.SECONDS] else {
            return false
        }
        guard let _ = workout[WorkoutKeys.MINUTES] else {
            return false
        }
        guard let _ = workout[WorkoutKeys.HOUR] else {
            return false
        }
        return true
    }
    
}
