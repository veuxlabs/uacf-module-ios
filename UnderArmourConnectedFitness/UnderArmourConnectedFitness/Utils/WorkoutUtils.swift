//
//  WorkoutUtils.swift
//  UnderArmourConnectedFitness
//
//  Created by Cesar Brenes on 5/3/17.
//  Copyright © 2017 Veux Labs. All rights reserved.
//

import Foundation


class WorkoutUtils {
    static let kilometerToMeters = 1000.0
    static let mileToMeters = 1609.34
    static let caloriesToJoules: Int = 4184
    static let minuteToSeconds: Int = 60
    static let secondsInHour = 3600.0
    
    
    
    class func getSpeedPreparedForUnderArmourAPI(workout: [String: Any]) -> NSNumber {
        if let workoutSpeedUnit = workout[WorkoutKeys.SPEED_UNIT], let workoutSpeedValue = workout[WorkoutKeys.SPEED_AVG] {
            let isInMPH = workoutSpeedUnit as! WorkoutSpeedUnit == WorkoutSpeedUnit.mph ? true : false
            return NSNumber(value: convertSpeedToMetersPerSecond(speedValue: workoutSpeedValue as! Double, isInMPH: isInMPH))
        }
        return NSNumber(value: 0.0)
    }
    
    
    class func getDistancePreparedForUnderArmourAPI(workout: [String: Any]) -> NSNumber {
        if let workoutDistanceUnit = workout[WorkoutKeys.DISTANCE_UNIT], let workoutValue = workout[WorkoutKeys.DISTANCE]{
            let isInMiles = workoutDistanceUnit as! WorkoutDistanceUnit == WorkoutDistanceUnit.miles ? true : false
            return NSNumber(value: convertDistanceToMeters(distanceValue: workoutValue as! Double, isInMiles: isInMiles))
        }
        return NSNumber(value: 0.0)
    }
    
    
    class func getMetabolicEnergyForUnderArmourAPI(workout: [String: Any]) -> NSNumber {
        if let metabolicEnergyUnit = workout[WorkoutKeys.METABOLIC_ENERGY_TOTAL_UNIT], let metabolicEnergyValue = workout[WorkoutKeys.METABOLIC_ENERGY_TOTAL]{
            let isInCalories = metabolicEnergyUnit as! WorkoutMetabolicEnergy == WorkoutMetabolicEnergy.calories ? true: false
            return NSNumber(value: convertMetabolicEnergyToJoules(metabolicEnergyValue: metabolicEnergyValue as! Int, isInCalories: isInCalories))
        }
        return NSNumber(value: 0)
    }
    
    
    class func getPowerAVGForUnderArmourAPI(workout: [String: Any]) -> NSNumber{
        if let speedAVG = workout[WorkoutKeys.POWER_AVG]{
            return NSNumber(integerLiteral: speedAVG as! Int)
        }
        return NSNumber(integerLiteral: 0)
    }
    
    class func getElapsedTimeForUnderArmourAPI(workout: [String: Any]) -> NSNumber{
        if let elapsedTimeValue = workout[WorkoutKeys.ELAPSED_TIME], let elapsedTimeUnit = workout[WorkoutKeys.ELAPSED_TIME_UNIT]{
            let isInSeconds = elapsedTimeUnit as! WorkoutElapsedTime == WorkoutElapsedTime.seconds ? true : false
            return NSNumber(integerLiteral: convertElapsedTimeToSeconds(elapsedTime: elapsedTimeValue as! Int, isInSeconds: isInSeconds))
        }
        return NSNumber(integerLiteral: 0)
    }
    
    
    class func getHeartRateAVGForUnderArmourAPI(workout: [String: Any]) -> NSNumber{
        if let hearthRateAVG = workout[WorkoutKeys.HEART_RATE_AVG]{
            return NSNumber(value: hearthRateAVG as! Int)
        }
        else{
           return NSNumber(value: 0)
        }
    }
    
    
    private class func convertSpeedToMetersPerSecond(speedValue: Double, isInMPH: Bool) -> Double{
        if isInMPH{
            return (speedValue * self.mileToMeters ) / secondsInHour
        }
        return (speedValue * self.kilometerToMeters) / secondsInHour
    }

    
    
    private class func convertDistanceToMeters(distanceValue: Double, isInMiles: Bool) -> Double{
        if isInMiles{
            return distanceValue * self.mileToMeters
        }
        return distanceValue * self.kilometerToMeters
    }
    
    private class func convertMetabolicEnergyToJoules(metabolicEnergyValue: Int, isInCalories: Bool) -> Int{
        if isInCalories{
            return metabolicEnergyValue * self.caloriesToJoules
        }
        return metabolicEnergyValue
    }
    
    private class func convertElapsedTimeToSeconds(elapsedTime: Int, isInSeconds: Bool) -> Int{
        if isInSeconds{
            return elapsedTime
        }
        else{
            return elapsedTime * self.minuteToSeconds
        }
    }
    
    
}
