/*
 * UALocationDerivedDataSource.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UADerivedDataSource.h"

@class UALocationDataPoint;

@interface UALocationDerivedDataSource : UADerivedDataSource <UADerivedDataSourceInterface>

@end
