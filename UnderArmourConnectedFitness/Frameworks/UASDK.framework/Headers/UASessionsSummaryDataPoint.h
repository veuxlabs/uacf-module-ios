//
//  UASessionsSummaryDataPoint.h
//  UASDK
//
//  Created by mallarke on 12/4/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UASessionsSummaryDataPoint : UADataPoint

/**
 * A summary of a sessions over a period of time (int)
 */
@property (nonatomic, readonly) NSNumber *sessionsSum;

@end
