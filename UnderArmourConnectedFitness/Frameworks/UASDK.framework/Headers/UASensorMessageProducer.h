/*
 * UASensorMessageProducer.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UAMessageProducer.h"

@class UADataPoint, UADataSourceIdentifier;

@interface UASensorMessageProducer : UAMessageProducer <UAMessageProducerInterface>

- (void)handleDataPoint:(UADataPoint *)dataPoint fromDataSourceIdentifier:(UADataSourceIdentifier *)dataSourceIdentifier;

@end
