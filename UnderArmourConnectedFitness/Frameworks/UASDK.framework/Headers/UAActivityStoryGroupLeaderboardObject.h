//
//  UAActivityStoryGroupLeaderboardObject.h
//  UASDK
//
//  Created by DX165 on 2014-11-24.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UAActivityStoryObject.h>

@class UAActivityStoryGroupLeaderboardResult, UAGroupLeaderboardListRef;

@interface UAActivityStoryGroupLeaderboardObject : UAActivityStoryObject

@property (nonatomic, strong) UAGroupLeaderboardListRef *groupLeaderboardListRef;

@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;

@property (nonatomic, strong) UAActivityStoryGroupLeaderboardResult *result;
@property (nonatomic, copy) NSArray *leaderboard;

@end
