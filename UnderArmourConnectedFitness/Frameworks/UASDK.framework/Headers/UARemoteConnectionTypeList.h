/*
 * UARemoteConnectionTypeList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@interface UARemoteConnectionTypeListRef : UAEntityListRef

+ (UARemoteConnectionTypeListRef *)remoteConnectionTypeListRef;

@end

@interface UARemoteConnectionTypeList : UAEntityList <UAEntityListRefInterface>

/**
 *  Reference to this object.
 */
@property (nonatomic, strong) UARemoteConnectionTypeListRef *ref;

/**
 *  A reference to the previous collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UARemoteConnectionTypeListRef *previousRef;

/**
 *  A reference to the next collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UARemoteConnectionTypeListRef *nextRef;

@end
