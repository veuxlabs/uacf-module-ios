/*
 * UAConfigurationMessage.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAMessage.h>

@class UARecorderContext;

/**
 *  A configuration message represents the change of state in a recording.
 */
@interface UAConfigurationMessage : UAMessage

/**
 *  A recording context.
 */
@property (nonatomic, readonly) UARecorderContext *context;

/**
 *  Creates a new message based on a context.
 *
 *  @param context A context for the recorder.
 *
 *  @return A new instance of a configuration message.
 */
- (instancetype)initWithContext:(UARecorderContext *)context NS_DESIGNATED_INITIALIZER;

@end
