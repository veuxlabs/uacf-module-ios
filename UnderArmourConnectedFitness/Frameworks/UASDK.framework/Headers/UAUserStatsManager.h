/*
 * UAUserStatsManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAManager.h>
#import <UASDK/UAUserStatsReport.h>

@class UAUserStatsReportRef, UADateRange;

@interface UAUserStatsManager : UAManager

/**
 *  Fetches the stats for a user
 *
 *  @param reference The reference to the user stats.
 *  @param response  A block called on completion.
 */
- (void)fetchUserStatsWithRef:(UAUserStatsReportRef *)reference
					 response:(UAObjectResponseBlock)response;

@end
