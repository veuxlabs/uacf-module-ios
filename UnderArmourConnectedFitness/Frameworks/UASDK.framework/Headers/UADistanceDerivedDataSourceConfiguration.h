/*
 * UADistanceDerivedDataSourceConfiguration.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UADerivedDataSourceConfiguration.h"

@interface UADistanceDerivedDataSourceConfiguration : UADerivedDataSourceConfiguration <UADataSourceConfigurationInterface>

@end
