/*
 * UAActivityTypeManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import "UAManager.h"

@class UAActivityType, UAActivityTypeReference, UAActivityTypeList, UAActivityTypeListRef;

typedef void (^UAActivityTypeResponseBlock)(UAActivityType *object, NSError *error);
typedef void (^UAActivityTypeListResponseBlock)(UAActivityTypeList *object, NSError *error);

@interface UAActivityTypeManager : UAManager

/**
 *  Fetches a UAActivityTypeList object with an array of ActivityType objects.
 *
 *  @param reference reference to the paged collection
 *  @param response A block called on completion
 */
- (void)fetchActivityTypesWithListRef:(UAActivityTypeListRef *)reference
                            response:(UAActivityTypeListResponseBlock)response;

- (void)fetchActivityTypesWithListRef:(UAActivityTypeListRef *)reference
                      withCachePolicy:(UAObjectCachePolicy)cachePolicy
                             response:(UAActivityTypeListResponseBlock)response;


/**
 *  Fetches a UAActivityType from a UAActivityTypeReference
 *
 *  @param reference reference to the activity type
 *  @param response A block called on completion.
 */
- (void)fetchActivityTypeWithRef:(UAActivityTypeReference *)reference
                        response:(UAActivityTypeResponseBlock)response;

- (void)fetchActivityTypeWithRef:(UAActivityTypeReference *)reference
                 withCachePolicy:(UAObjectCachePolicy)cachePolicy
                        response:(UAActivityTypeResponseBlock)response;

- (void)fetchAllActivityTypes:(UAActivityTypeListResponseBlock)response;
- (void)fetchAllActivityTypes:(UAObjectCachePolicy)cachePolicy response:(UAActivityTypeListResponseBlock)response;

@end
