/*
 * UAGroupInvite.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>
#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@class UAUserRef;
@class UAGroupRef;

@interface UAGroupInviteRef : UAEntityRef

/**
* Ref builder based on the group invite's ID.
*/
+ (instancetype)groupInviteRefWithGroupInviteID:(NSString *)groupInviteID;

@end

@interface UAGroupInvite : UAEntity <UAEntityInterface, NSCoding>

/**
* Reference to the group invite.
* @return Reference to the group invite.
*/
@property (nonatomic, strong) UAGroupInviteRef *ref;

/**
* The group ref associated with the invite.
* @return The group ref associated with the invite.
*/
@property (nonatomic, strong) UAGroupRef *groupRef;

/**
* The user ref associated with the invite.
* @return The user ref associated with the invite.
*/
@property (nonatomic, strong) UAUserRef *userRef;

/**
* The email associated with the invite.
* @return The email associated with the invite.
*/
@property (nonatomic, strong) NSString *email;

@end
