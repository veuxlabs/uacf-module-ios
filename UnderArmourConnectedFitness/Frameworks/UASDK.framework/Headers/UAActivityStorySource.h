//
//  UAActivityStorySource.h
//  UASDK
//
//  Created by Jeremiah Smith on 10/23/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UAActivityStorySource : NSObject

@property (nonatomic, strong) NSNumber *sourceID;

@property (nonatomic, strong) NSString *siteName;

@property (nonatomic, strong) NSString *siteURL;

@end
