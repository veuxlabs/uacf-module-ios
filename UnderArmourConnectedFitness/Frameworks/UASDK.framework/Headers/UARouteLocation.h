/*
 * UARouteLocation.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

/**
 *  An object that represents a point, consisting of latitude and longitude coordinates,
 *  as well as point elevation and distance that this point takes place in a route.
 */
@interface UARouteLocation : NSObject

/**
 *  The distance where this point is located within a route.
 */
@property (nonatomic, strong) NSNumber *distance;

/**
 *  The elevation where this point resides.
 */
@property (nonatomic, strong) NSNumber *elevation;

/**
 *  The latitude of this point.
 */
@property (nonatomic, strong) NSNumber *latitude;

/**
 *  The longitude of this point.
 */
@property (nonatomic, strong) NSNumber *longitude;

/**
 *  The CLLocationCoordinate2D representation of this object.
 */
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

@end
