/*
 * UAPrivacyRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntityRef.h>
#import <UASDK/UAEnumerations.h>

/**
 *  An object that references a privacy setting.
 */
@interface UAPrivacyRef : UAEntityRef <NSCoding>

+ (UAPrivacyRef *)privacyRefWithPrivacyType:(UAPrivacyType)privacyType;

@property (nonatomic, copy) NSString *name;
@property (nonatomic, readonly) UAPrivacyType privacyType;

@end
