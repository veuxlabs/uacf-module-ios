/*
 * UALocationDataSourceConfiguration.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UADataSourceConfiguration.h"

/**
 *  A producer configuration used for Apple's Location Services.
 */
@interface UALocationDataSourceConfiguration : UADataSourceConfiguration <UADataSourceConfigurationInterface>

@end
