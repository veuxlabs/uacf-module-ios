/*
 * UALogger.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>

/**
 *  A class that allows for logging framework information at different levels.
 *  A brief overview of the levels of logging, by ascending severity:
 *
 *
 *  Log
 *  ------
 *  For use in cases where debug-level information needs to be logged.
 *  Use this for debugging purposes, or for things you don't necessarily
 *  need to persist in a log file.
 *
 *  Write
 *  ------
 *  For use in cases where you would like to write debug-level information to
 *  a local file.
 *
 *  Info
 *  ------
 *  For use in cases where an event has happened. Some examples include
 *  transitioning to a new view, recording an analytics event, or the user
 *  tapping a button to trigger an action.
 *
 *  Warning
 *  ------
 *  For use in cases where an issue has occurred, but can be
 *  rectified. These kinds of issues are usually unintended, but don't
 *  necessarily deteriorate the overall performance and usage of the framework.
 *
 *  Error
 *  ------
 *  For use in rare cases where an issue has occurred that cannot be rectified.
 *  These issues are ones that may cause exceptions, unintended behavior, or
 *  may cause an app to crash for any reason.
 *
 *
 *  Each function is a wrapper for NSLog and will output the value to the console
 *  (unless the framework was built against a profile other than Debug).
 *
 *  Methods with severity level Write and stronger will additionally write their
 *  logs to a local file. This file can be used for debugging and system information
 *  purposes.
 */
@interface UALogger : NSObject

/**
 *  UALog takes a format string and an arbitrary number of arguments and outputs the result into the console.
 *  If the build is not built against the Debug profile, nothing will display in the console.
 */
#define UALog( s, ... ) [UALogger log:__FILE__ function:__PRETTY_FUNCTION__ lineNumber:__LINE__ format:(s), ## __VA_ARGS__];

/**
 *  UAWrite takes a format string and an arbitrary number of arguments and outputs the result into both the
 *  console and a local file. If the build is not built against the Debug profile, nothing will display in the console.
 *  However, the result will still be written into the local log file.
 */
#define UAWrite( s, ... ) [UALogger write:__FILE__ function:__PRETTY_FUNCTION__ lineNumber:__LINE__ format:(s), ## __VA_ARGS__];

/**
 *  UAInfo works the same as UAWrite, along with the addition of adding the result to the Crashlytics log.
 *  These appear in individual crash logs.
 *  UAInfo should only be used for events that have happened (i.e. analytics, user interaction events of interest, etc).
 */
#define UAInfo( s, ... ) [UALogger info:__FILE__ function:__PRETTY_FUNCTION__ lineNumber:__LINE__ format:(s), ## __VA_ARGS__];

/**
 *  UAWarning works the same as UAInfo, except that the log is prepended with 'Warning:'.
 *  UAWarning should only be used when something unintended happens,
 *  but does not necessarily deteriorate the performance or usability of the app.
 */
#define UAWarning( s, ... ) [UALogger warning:__FILE__ function:__PRETTY_FUNCTION__ lineNumber:__LINE__ format:(s), ## __VA_ARGS__];

/**
 *  UAError works the same as UAWarning, except that the log is prepended with 'ERROR:'.
 *  UAError should only be used when an event in the app creates an exception,
 *  causes a crash, or otherwise does something unexpected that negatively impacts
 *  the performance and usability of the app.
 */
#define UALogError( s, ... ) [UALogger error:__FILE__ function:__PRETTY_FUNCTION__ lineNumber:__LINE__ format:(s), ## __VA_ARGS__];

+ (UALogger *)sharedInstance;
- (void)clearLog;

+ (void)log:(char *)file function:(const char *)function lineNumber:(int)lineNumber format:(NSString *)formatString, ...;
+ (void)write:(char *)file function:(const char *)function lineNumber:(int)lineNumber format:(NSString *)formatString, ...;
+ (void)info:(char *)file function:(const char *)function lineNumber:(int)lineNumber format:(NSString *)formatString, ...;
+ (void)warning:(char *)file function:(const char *)function lineNumber:(int)lineNumber format:(NSString *)formatString, ...;
+ (void)error:(char *)file function:(const char *)function lineNumber:(int)lineNumber format:(NSString *)formatString, ...;

@end
