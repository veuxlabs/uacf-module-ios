//
//  UAHeartRateDataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/20/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UAHeartRateDataPoint : UADataPoint

/**
 *  Heart rate (double, beats per minute).
 */
@property (nonatomic, readonly) NSNumber *heartRate;

/**
 *  Instantiates a heart rate data point.
 *  @param heartRate The heart rate in beats per minute. Must not be nil.
 *  @return A newly instantiated data point.
 */
- (instancetype)initWithHeartRate:(NSNumber *)heartRate NS_DESIGNATED_INITIALIZER;

@end
