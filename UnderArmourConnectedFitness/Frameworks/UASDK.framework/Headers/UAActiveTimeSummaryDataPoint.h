//
//  UAActiveTimeSummaryDataPoint.h
//  UASDK
//
//  Created by mallarke on 11/26/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UAActiveTimeSummaryDataPoint : UADataPoint

/**
 * A summary of a user's activity over a period of time (double, seconds)
 */
@property (nonatomic, readonly) NSNumber *activeTimeSum;

@end
