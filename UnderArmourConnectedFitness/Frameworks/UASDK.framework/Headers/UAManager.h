/*
 * UAManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

@import Foundation;

typedef NS_ENUM(NSInteger, UAManagerErrorCode) {
	UAManagerErrorCodeMissingParameter,
	UAManagerErrorCodeWrongJSONResponseFormat,
	UAManagerErrorCodeErrorInRequest,
    UAManagerErrorInvalidDataElement
};

typedef NS_ENUM(NSInteger, UAObjectCachePolicy) {
    UACacheElseNetwork = 0,
    UANetworkElseCache = 1,
    UACacheOnly = 3,
    UANetworkOnly = 4,
};

@class UAOAuthSession, AFHTTPRequestOperation, AFHTTPClient, AFMultipartFormData;

@interface UAManager : NSObject

- (instancetype)init __attribute__((unavailable("init not available")));

+ (instancetype)new __attribute__((unavailable("new not available")));

@end
