//
//  UAActivityStoryUserActor.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAActivityStoryActor.h>
#import <UASDK/UAUser.h>

@interface UAActivityStoryUserActor : UAActivityStoryActor

@property (nonatomic, strong) UAUserRef *ref;

@property (nonatomic, copy) NSString *actorType;

@property (nonatomic, copy) NSString *firstName;

@property (nonatomic, copy) NSString *lastName;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, assign) UAGender gender;

@property (nonatomic, strong) UAFriendshipSummary *friendship;

@end
