/*
 * UAUserList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@class UAUserRef;

/**
 *  An object that references a collection of users.
 */
@interface UAUserListRef : UAEntityListRef

+ (UAUserListRef *)userListRefForUsersWithName:(NSString *)name
                                         limit:(NSUInteger)limit;

// Builds a reference to fetch friends
+ (UAUserListRef *)friendsListRefForUser:(UAUserRef *)user
                                   limit:(NSUInteger)limit;

+ (UAUserListRef *)friendsListRefForUser:(UAUserRef *)user;

// Builds a reference to fetch pending friends
+ (UAUserListRef *)pendingFriendsListRefForUser:(UAUserRef *)user
                                          limit:(NSUInteger)limit;

+ (UAUserListRef *)pendingFriendsListRefForUser:(UAUserRef *)user;

/**
 *  Gets a list of user references including searching by email address
 *  to find friends the user already knows.
 *
 *  @param userRef The user for which the API will suggest friends.
 *
 *  @param addresses A list of email addresses (as NSString) from the user's contacts.
 *  The API will search for users with matching email addresses and suggest them as
 *  friends.
 *
 *  @return An NSArray of {@link UAUserListRef}, each of which references
 *  suggested friends whose emails are included in the provided list.
 *
 *  @discussion There is no limit to the number of email addresses which can be
 *  submitted to this call, as they are batched into separate list refs.
 */
+ (NSArray *)getUserListRefsForUserRef:(UAUserRef *)userRef forEmailAddresses:(NSArray *)emails;

@end

@interface UAUserList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UAUserListRef *ref;

/**
 *  A reference to the previous collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UAUserListRef *previousRef;

/**
 *  A reference to the next collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UAUserListRef *nextRef;

@end
