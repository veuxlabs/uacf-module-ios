/*
 * UADateRange.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>

@interface UADateRange : NSObject
@property (nonatomic, strong) NSDate *start;
@property (nonatomic, strong) NSDate *end;
@end

NS_INLINE UADateRange* UADateRangeMake(NSDate *start, NSDate *end)
{
    UADateRange *r = [[UADateRange alloc] init];
    r.start = start;
    r.end = end;
    return r;
}
