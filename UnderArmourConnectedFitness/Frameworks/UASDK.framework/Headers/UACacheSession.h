/*
 * UACacheSession.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>

/**
 *  Pending opetion bitfield for cached objects
 */
typedef NS_OPTIONS(NSUInteger, UACacheEntityPendingOperation) {
	/**
	 *  No pending operation on the cached object
	 */
	UACacheEntityPendingOperationNone   = 0,
	/**
	 *  Pending creation operation on the cached object
	 */
	UACacheEntityPendingOperationCreate = 1 << 0,
};

/**
 *  Cache close specification
 */
typedef NS_ENUM(NSUInteger, UACacheCloseStrategy) {
	/**
	 *  Closes the cache handle without removing any data from the cache
	 */
	UACacheCloseLeaveCache,
	/**
	 *  Closes the cache handle and clears all data in the cache
	 */
	UACacheCloseClearAllCache,
	/**
	 *  Closes the cache handle and clears all objects without pending operations
	 */
	UACacheCloseClearNoPendingOps,
};

/**
 *  The protocol needed to be implemenetd by an object which can reference
 *  a cached object.
 */
@protocol UACacheableObjectReference <NSObject>

/**
 *  This is a identifier which will be read by the cached manager to "look up" an object. It is
 *  expected this is unique per cached object (per type). The cache is expected to set this.
 */
@property (nonatomic, readonly) NSString *remoteCacheId;

/**
 *  This is a cache ID that the cache manager will use as an opaque handle to reference an object. The implementor
 *  should never set or need to read this property.
 */
@property (nonatomic, strong) id localCacheId;

/**
 *  Internal property used to track various cache options for an object
 */
@property (nonatomic, assign) NSUInteger cacheOptions;

@end

/**
 *  The protocol required for objects that have relationships to the actual objects that need to be pulled from cache.
 *  If a object only has a reference to the other object, then this protocol isn't required.
 */
@protocol UACacheableCollectionObject <NSObject>

/**
 *  The array of objects. This will be set by the cache manager when pull an object
 *  from cache.
 */
@property (nonatomic, strong) NSArray *cacheableObjects;

/**
 *  References to the objects. There should be a 1:1 oredered mapping between a reference and the objects in cacheableObjects.
 */
@property (nonatomic, readonly) NSArray *cacheableObjectReferences;

@end

@interface UACachedObjectMeta : NSObject

@property (nonatomic, strong) NSDate *timeCached;

@property (nonatomic, assign) UACacheEntityPendingOperation pendingOperations;

@end

@class UAEntityRef, UAEntityListRef;

typedef void (^FetchResponseBlock)(id object);

@protocol UACacheSessionInterface <NSObject>

/**
 *  Opens a cache session
 *
 *  @param identifier An optional identififer to uniquely associate this cache.
 */
- (void)openSessionWithIdentifier:(NSString *)identifier;

/**
 *  Closes a cache session
 *
 *  @param cacheStrategy The strategy to use when closing the session.
 */
- (void)closeSession:(UACacheCloseStrategy)cacheStrategy;

/**
 *  Flushes and waits for any pending cache transactions to finish.
 */
- (void)synchronize;

/**
 *  Fetches a cached object from the cache
 *
 *  @param reference The reference to the object
 *  @param aClass    The class of the object
 *
 *  @return Returns the object if it exists and nil if the object is not found
 */
- (id)fetchCachedObjectWithReference:(id <UACacheableObjectReference>)reference
                            forClass:(Class)aClass;

/**
 *  Fetches a cached object from the cache
 *
 *  @param reference   The reference to the object
 *  @param aClass      The class of the object
 *  @param resultBlock The callback block that will return the object if it exists, nil if not.
 *
 */
- (void)fetchAsyncCachedObjectWithReference:(id <UACacheableObjectReference>)reference
                                   forClass:(Class)aClass
                                     result:(FetchResponseBlock)resultBlock;

/**
 *  Adds an object to the cache with no pending operations. 
 *
 *  @discussion If the object already exists in the cache for this reference, it will be overwritten with this object.
 *
 *  @param object    The object to be cached.
 *  @param reference The reference of the object to be cached.
 */
- (void)cacheObject:(id)object withReference:(id <UACacheableObjectReference>)reference;

/**
 *  Adds an object to the cache.
 *
 *  @discussion If the object already exists in the cache for this reference, it will be overwritten with this object.
 *
 *  @param object    The object to be cached.
 *  @param reference The reference of the object to be cached.
 *  @param operation The pending operation type to set for this object.
 */
- (void)cacheObject:(id)object withReference:(id <UACacheableObjectReference>)reference withPendingOperation:(UACacheEntityPendingOperation)operation;

/**
 *  Removes an object from the cache. If the object can't be found, this will do nothing.
 *
 *  @param reference The reference to the object to remove.
 *  @param aClass    The class of the object to remove.
 */
- (void)removeObjectWithReference:(id <UACacheableObjectReference>)reference forClass:(Class)aClass;

/**
 *  Fetches ojects for a given class with a certain pending operation set.
 *
 *  @param operation The type of operation for fetch objects for.
 *  @param aClass    The class of the objects to fetch.
 *
 *  @return The list of objects.
 */
- (NSArray *)fetchCachedObjectsWithPendingOperation:(UACacheEntityPendingOperation)operation
										   forClass:(Class)aClass;

/**
 *  Fetch the meta data for a particular cached object.
 *
 *  @param reference The reference to the object
 *  @param aClass    The class of the object
 *
 *  @return The meta object for the cached object or nil if the object is not found.
 */
- (UACachedObjectMeta *)fetchCacheMetaForObjectWithReference:(id <UACacheableObjectReference>)reference forClass:(Class)aClass;

/**
 *  Fetch the meta data for a particular cached object.
 *
 *  @param reference The reference to the object
 *  @param aClass    The class of the object
 *  @param resultBlock The meta object for the cached object or nil if the object is not found.
 */
- (void)fetchAsyncCacheMetaForObjectWithReference:(id <UACacheableObjectReference>)reference
										 forClass:(Class)aClass
										   result:(void (^)(UACachedObjectMeta *meta))resultBlock;

@end
