/*
 * UAOAuth2CredentialsMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>

@class UAOAuth2Credentials;

@interface UAOAuth2CredentialsMapper : NSObject

+ (UAOAuth2Credentials *)oAuth2CredentialsFromResponse:(NSDictionary *)response;

@end
