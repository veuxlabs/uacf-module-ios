/*
 * UAEntitySyncMeta.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

@import Foundation;

@class UAEntitySyncMeta, UAWorkoutRef;

@protocol UAEntitySyncInterface <NSObject>

- (void)observeSyncState:(UAEntitySyncMeta *)syncMeta forWorkoutRef:(UAWorkoutRef *)reference;

@end

/**
 *  The sync state of the entity
 */
typedef NS_ENUM(NSUInteger, UAEntitySyncState)
{
	/**
	 *  Entity is not synced and is not currently be synced
	 */
	UAEntitySyncStateNotSynced = 0,
	/**
	 *  Entity is currently being synced
	 */
	UAEntitySyncStateSyncing = 1,
	/**
	 *  Entity is synced
	 */
	UAEntitySyncStateSynced = 2,
};

@interface UAEntitySyncMeta : NSObject <NSCoding>

+ (instancetype)entitySyncMetaWithState:(UAEntitySyncState)state;

@property (nonatomic, readonly) UAEntitySyncState state;

@end
