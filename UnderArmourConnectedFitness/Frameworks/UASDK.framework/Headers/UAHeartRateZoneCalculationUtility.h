/*
 * UAHeartRateZoneCalculationUtility.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#ifndef __UASDK__UAHeartRateCalculationUtility__
#define __UASDK__UAHeartRateCalculationUtility__

#include <stdio.h>

typedef struct UAHeartRateZoneStruct
{
	char *name;
	int64_t start;
	int64_t end;
} UAHeartRateZoneStruct;

UAHeartRateZoneStruct *calculateHeartRateZonesWithAge(int64_t age, int64_t *count);
UAHeartRateZoneStruct *calculateHeartRateZonesWithMax(int64_t maxHeartRate, int64_t *count);

#endif /* defined(__UASDK__UAHeartRateCalculationUtility__) */
