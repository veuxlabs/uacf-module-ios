/*
 * UAWorkoutManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAManager.h>
#import <UASDK/UAEntitySyncMeta.h>

@class UAWorkoutListRef, UAWorkoutRef, UAWorkout, UAWorkoutList;

typedef void (^UAWorkoutResponseBlock)(UAWorkout *object, NSError *error);
typedef void (^UAWorkoutListResponseBlock)(UAWorkoutList *object, NSError *error);
typedef void (^UAUnsycnedWorkoutsResponseBlock)(NSArray *workouts, NSArray *syncMeta, NSError *error);

@interface UAWorkoutManager : UAManager

- (void)fetchWorkoutWithRef:(UAWorkoutRef *)reference
                    response:(UAWorkoutResponseBlock)response;

- (void)fetchWorkoutWithRef:(UAWorkoutRef *)reference
            withCachePolicy:(UAObjectCachePolicy)cachePolicy
                   response:(UAWorkoutResponseBlock)response;

- (void)fetchWorkoutsWithListRef:(UAWorkoutListRef *)reference
                    response:(UAWorkoutListResponseBlock)response;

- (void)fetchWorkoutsWithListRef:(UAWorkoutListRef *)reference
            withCachePolicy:(UAObjectCachePolicy)cachePolicy
                   response:(UAWorkoutListResponseBlock)response;

- (void)createWorkout:(UAWorkout *)workout
             response:(UAWorkoutResponseBlock)response;

- (void)updateWorkout:(UAWorkout *)workout
             response:(UAWorkoutResponseBlock)response;

- (void)deleteWorkout:(UAWorkoutRef *)reference
             response:(UAResponseBlock)response;

#pragma mark offline sync methods
- (void)fetchUnsyncedWorkouts:(UAUnsycnedWorkoutsResponseBlock)response;
- (void)addSyncObserver:(id <UAEntitySyncInterface>)observer forWorkoutRef:(UAWorkoutRef *)reference;
- (void)removeSyncObserver:(id <UAEntitySyncInterface>)observer;

@end
