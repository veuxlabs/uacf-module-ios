/*
 * UAPageTypeMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UAPageType.h"
#import "UAPageTypeRef.h"
#import "UAPageAssociationList.h"
#import "UAPageAssociationRef.h"

@interface UAPageTypeMapper : NSObject

+ (UAPageType *)objectFromDictionary:(NSDictionary *)pageTypeDictionary;

@end
