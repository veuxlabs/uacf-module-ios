/*
 * UAGroupLeaderboardList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@class UADateRange;

@interface UAGroupLeaderboardListRef : UAEntityListRef

+ (instancetype)groupLeaderboardListRefWithGroupID:(NSString *)groupID;

// iteration is a zero based index
+ (instancetype)groupLeaderboardListRefWithGroupID:(NSString *)groupID iteration:(NSUInteger)iteration;

@end

@interface UAGroupLeaderboardList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UAGroupLeaderboardListRef *ref;

/**
 * A reference to the previous collection of group leaderboards.
 * @discussion If no collection has previously been visited, this value will be nil.
 */
@property (nonatomic, strong) UAGroupLeaderboardListRef *previousRef;

/**
 * A reference to the next collection of group leaderboards.
 * @discussion If no more group leaderboards exist, this value will be nil.
 */
@property (nonatomic, strong) UAGroupLeaderboardListRef *nextRef;

/**
 * A reference to the previous iteration of group leaderboards.
 * @discussion If no previous iteration is available, this value will be nil.
 */
@property (nonatomic, strong) UAGroupLeaderboardListRef *previousIteration;

/**
 * A reference to the next iteration of group leaderboards.
 * @discussion If no next iteration is available, this value will be nil.
 */
@property (nonatomic, strong) UAGroupLeaderboardListRef *nextIteration;

@end
