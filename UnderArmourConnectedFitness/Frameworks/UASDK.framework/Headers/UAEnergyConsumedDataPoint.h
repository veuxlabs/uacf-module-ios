//
//  UAEnergyConsumedDataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/20/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UAEnergyConsumedDataPoint : UADataPoint

/**
 *  Energy consumed (double, joules).
 */
@property (nonatomic, readonly) NSNumber *energyConsumed;

/**
 *  Instantiate an energy consumed data point.
 *  @param energyConsumed Energy consumed in joules. Must not be nil.
 *  @return An instantiated data point.
 */
- (instancetype)initWithEngeryConsumed:(NSNumber *)energyConsumed NS_DESIGNATED_INITIALIZER;

@end
