/*
 * UAActivityType.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@interface UAActivityTypeReference : UAEntityRef

/**
 *  Creates a reference to an activity type with its entityId
 */
+ (instancetype)activityTypeRefWithEntityId:(NSString *)entityId;

@end

@interface UAActivityType : UAEntity <NSCoding, UAEntityInterface>

/**
 *  Reference to this activity type
 */
@property (nonatomic, strong) UAActivityTypeReference *ref;

/**
 *  Reference to the parent activity type. This will be nil if there is no parent.
 */
@property (nonatomic, strong) UAActivityTypeReference *parentActivityRef;

/**
 *  Reference to the root activity type. If the activity type is a root, then this will
 *  be a reference to itself.
 */
@property (nonatomic, strong) UAActivityTypeReference *rootActivityRef;

/**
 *  Name of the activity type
 */
@property(nonatomic, copy) NSString *name;

/**
 *  The display name for this activity. This may be nil.
 */
@property(nonatomic, copy) NSString *shortName;

/**
 *  The default met value for this activity
 */
@property(nonatomic, assign) double mets;

/**
 *  An array of UAActivityTypeMetsSpeed objects to determine the mets at a particular speed. 
 *  This property may be nil if there is not speed table associated with an activity.
 */
@property(nonatomic, strong) NSArray *metsSpeed;

/**
 *  The URL for the image icon of the activity
 */
@property (nonatomic, strong) NSURL *iconURL;

/**
 *  Will be true if there are child activity types assocated with this activity.
 */
@property (nonatomic, assign) BOOL hasChildren;

@end


@interface UAActivityTypeMetsSpeed : NSObject <NSCoding>

@property (nonatomic, assign) double mets;

@property (nonatomic, assign) double speed;

@end
