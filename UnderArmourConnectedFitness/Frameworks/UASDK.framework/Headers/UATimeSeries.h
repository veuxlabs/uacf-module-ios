/*
 * UATimeSeries.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>

@class UATimeSeriesPoint;

@protocol UATimeSeriesProtocol <NSObject>

- (void)addPoint:(UATimeSeriesPoint *)point;

- (UATimeSeriesPoint *)pointAtIndex:(NSUInteger)index;

@end

@interface UATimeSeries : NSObject <NSCoding, UATimeSeriesProtocol>

- (instancetype)initWithCapacity:(NSUInteger)capacity;

@property (nonatomic, readonly) NSUInteger count;

- (void)sort;

- (void)enumerateTimeSeriesUsingBlock:(void (^)(UATimeSeriesPoint *point, NSUInteger idx, BOOL *stop))block;

@end
