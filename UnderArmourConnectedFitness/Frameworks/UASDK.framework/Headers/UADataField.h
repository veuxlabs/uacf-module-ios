//
//  UADataField.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/19/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UADataUnits;

typedef NS_ENUM(NSInteger, UADataFieldType)
{
	UADataFieldTypeInteger, UADataFieldTypeDouble
};

@interface UADataField : NSObject <NSCoding>

@property (nonatomic, readonly) NSString *dataFieldID;
@property (nonatomic, readonly) UADataUnits *units;
@property (nonatomic, readonly) UADataFieldType type;

- (instancetype)init __attribute__((unavailable("init not available")));
+ (instancetype)new __attribute__((unavailable("new not available")));

@end
