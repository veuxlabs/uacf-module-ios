/*
 * UADataPointMessage.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAMessage.h>

@class UADataPoint, UADataTypeRef, UADataSourceIdentifier;

@interface UADataPointMessage : UAMessage

/**
 *  The data point for this message.
 */
@property (nonatomic, readonly) UADataPoint *dataPoint;

/**
 *  The type of data this message produces.
 */
@property (nonatomic, readonly) UADataTypeRef *dataTypeRef;

/**
 *  The data source identity for this message.
 */
@property (nonatomic, readonly) UADataSourceIdentifier *dataSourceIdentifier;

/**
 *  Instantiates a new UADataPointMessage with a given UADataPoint.
 *
 *  @param dataPoint A data point to include in a message.
 *
 *  @return An instantiation of a UADataPointMessage.
 */
- (instancetype)initWithDataPoint:(UADataPoint *)dataPoint dataSourceIdentifier:(UADataSourceIdentifier *)dataSourceIdentifier;

@end
