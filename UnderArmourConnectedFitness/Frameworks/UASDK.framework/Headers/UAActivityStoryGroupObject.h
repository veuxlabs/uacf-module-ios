//
//  UAActivityStoryGroupObject.h
//  UASDK
//
//  Created by DX165 on 2014-11-24.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UASDK.h>
#import <UASDK/UAActivityStoryObject.h>
#import <UASDK/UAGroup.h>
#import <UASDK/UAGroupInviteList.h>
#import <UASDK/UAGroupUserList.h>
#import <UASDK/UADataType.h>
#import <UASDK/UAPeriod.h>

@interface UAActivityStoryGroupObject : UAActivityStoryObject

@property (nonatomic, strong) UAGroupRef *groupRef;

@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) UADataTypeRef *dataTypeRef;
@property (nonatomic, copy) NSString *dataTypeField;
@property (nonatomic) BOOL inviteAccepted;

@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;
@property (nonatomic, strong) UAPeriod *period;

@property (nonatomic, strong) UAGroupPurposeRef *groupPurposeRef;

@property (nonatomic, strong) UAGroupInviteListRef *groupInviteListRef;
@property (nonatomic) NSUInteger groupInviteCount;

@property (nonatomic, strong) UAGroupUserListRef *groupUserListRef;
@property (nonatomic) NSUInteger groupUserCount;

@end
