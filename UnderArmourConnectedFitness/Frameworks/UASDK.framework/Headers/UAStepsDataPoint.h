//
//  UAStepsDataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/20/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UAStepsDataPoint : UADataPoint

/**
 *  Number of steps (NSInteger, unitless).
 */
@property (nonatomic, readonly) NSNumber *steps;

/**
 *  Initialize a steps data point.
 *  @param steps The number of steps. Must not be nil
 *  @return Initialized data point
 */
- (instancetype)initWithSteps:(NSNumber *)steps;

@end
