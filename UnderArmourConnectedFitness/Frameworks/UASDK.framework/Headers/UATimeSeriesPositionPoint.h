/*
 * UATimeSeriesPositionPoint.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


@import CoreLocation;

#import <UASDK/UATimeSeriesPoint.h>

/**
 *  If this object is created with an array of data points its epxected it the values array should be as follows:
 *  
 *  [0] double - If this isn't present on this point, a value of NaN must still be supplied.
 *  [1] double - lat - optional
 *  [2] double - long - optional
 */
@interface UATimeSeriesPositionPoint : UATimeSeriesPoint

+ (instancetype)pointAtTime:(NSTimeInterval)timestamp withElevation:(double)elevation withCoordinate:(CLLocationCoordinate2D)coordinate;

+ (instancetype)pointAtTime:(NSTimeInterval)timestamp withCoordinate:(CLLocationCoordinate2D)coordinate;

+ (instancetype)pointAtTime:(NSTimeInterval)timestamp withElevation:(double)elevation;

/**
 *  Elevation in meters
 */
@property (nonatomic, readonly) double elevation;

@property (nonatomic, readonly) BOOL hasElevation;

/**
 *  The coordinate for this point.
 */
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

@end
