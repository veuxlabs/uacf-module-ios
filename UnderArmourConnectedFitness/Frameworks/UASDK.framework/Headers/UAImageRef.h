/*
 * UAImageRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import "UAEntityRef.h"

/**
 *  An object that references an image.
 */
@interface UAImageRef : UAEntityRef <NSCoding>

@property (nonatomic, copy) NSString *name;

@end
