//
//  UAActivityStoryActor.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAFriendshipSummary.h>
#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@interface UAActivityStoryActor : UAEntity <UAEntityInterface>

/**
 The reference of this actor.
 */
@property (nonatomic, strong) UAEntityRef *ref;

@end
