//
//  UAActivityStoryCommentList.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAEntityList.h>

@interface UAActivityStoryCommentList : UAEntityList

@property (nonatomic, strong) NSArray *comments;

/**
 @return YES if the currently authenticated user has commented on the associated ActivityStory.
 */
@property (nonatomic) BOOL replied;

@end
