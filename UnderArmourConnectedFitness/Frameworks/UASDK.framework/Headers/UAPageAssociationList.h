/*
 * UAPageAssociationList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAPageAssociationRef.h>
#import <UASDK/UAEntityList.h>
#import <UASDK/UAPageAssociationListRef.h>
#import <UASDK/UAPageAssociation.h>

@interface UAPageAssociationList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UAPageAssociationListRef *ref;

@property (nonatomic, strong) UAPageAssociationListRef *previousRef;

@property (nonatomic, strong) UAPageAssociationListRef *nextRef;

@end
