/*
 * UAPageMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAObjectMapper.h>

@class UAPage, UAPageAssociation, UAPageType, UAPageTypeList, UAPageAssociationList, UAPageList;

/**
 *  A mapping class that transforms a page web response into its respective
 *  page model.
 */
@interface UAPageMapper : UAObjectMapper

/**
 *  Takes a response and transforms it into an UAPage.
 *
 *  @param dictionary A web service response.
 *
 *  @return An UAPage representation of this response.
 */
+ (UAPage *)pageFromResponse:(NSDictionary *)dictionary;

/**
 *  Takes a response and transforms it into an UAPageTypeList.
 *
 *  @param dictionary A web service response.
 *
 *  @return An UAPageTypeList of this response.
 */
+ (UAPageTypeList *)pageTypeListFromResponse:(NSDictionary *)dictionary;

/**
 *  Takes a response and transforms it into an UAPageType.
 *
 *  @param dictionary A web service response.
 *
 *  @return An UAPageType of this response.
 */
+ (UAPageType *)pageTypeFromResponse:(NSDictionary *)dictionary;

/**
 *  Takes a response and transforms it into an UAPageAssociationList.
 *
 *  @param dictionary A web service response.
 *
 *  @return An UAPageAssociationList of this response.
 */
+ (UAPageAssociationList *)pageAssociationListFromResponse:(NSDictionary *)dictionary;

+ (UAPageList *)pageListFromResponse:(NSDictionary *)dictionary;

@end
