/*
 * UAMessage.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>
#import "UADataSourceIdentifier.h"

@class UARecorderCalculator;

/**
 *  The base class for a message that contains data from a message producer.
 */
@interface UAMessage : NSObject

/**
 *  A timestamp of when this message was created.
 */
@property (nonatomic, assign, readonly) NSTimeInterval timestamp;

/**
 *  A producer identifier of where this message came from.
 */
@property (nonatomic, weak, readonly) UADataSourceIdentifier *dataSourceIdentifier;

/**
 *  A method used when a UARecordProcessor is processing a message.
 *
 *  @param recorderCalculator A session calculator used to process this message's data.
 *
 *  @discussion This method must be implemented for all subclasses
 */
- (void)processMessage:(UARecorderCalculator *)recorderCalculator;

@end
