/*
 * UAUserAchievementList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@interface UAUserAchievementListRef : UAEntityListRef

@end

@interface UAUserAchievementList : UAEntityList

@property (nonatomic, strong) UAUserAchievementListRef *previousRef;

@property (nonatomic, strong) UAUserAchievementListRef *nextRef;

@end
