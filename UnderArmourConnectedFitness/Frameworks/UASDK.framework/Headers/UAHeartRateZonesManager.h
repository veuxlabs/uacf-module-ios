/*
 * UAHeartRateZonesManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAManager.h>

@class UAHeartRateZones;
@class UAHeartRateZonesRef;
@class UAHeartRateZonesList;
@class UAHeartRateZonesListRef;

typedef void (^UAHeartRateZonesResponseBlock)(UAHeartRateZones *object, NSError *error);
typedef void (^UAHeartRateZonesListResponseBlock)(UAHeartRateZonesList *object, NSError *error);

@interface UAHeartRateZonesManager : UAManager

/**
 *	Fetches a UAHeartRateZones object 
 *
 * @param reference A reference to the UAHeartRateZones object
 * @param response A block called on completion
 */
- (void)fetchHeartRateZonesWithRef:(UAHeartRateZonesRef *)reference response:(UAHeartRateZonesResponseBlock)response;

/**
 *	Fetches a UAHeartRateZonesList object with an array of UAHeartRateZones objects.
 *
 * @param reference A reference to the UAHeartRateZonesList object
 * @param response A block called on completion
 */
- (void)fetchHeartRateZonesListWithRef:(UAHeartRateZonesListRef *)reference response:(UAHeartRateZonesListResponseBlock)response;

/**
 * Creates heart rate zones
 * 
 * @param zones An Array of heart rate zones to create
 * @param response A block called on completion
 */
- (void)createHeartRateZones:(UAHeartRateZones *)zones response:(UAHeartRateZonesResponseBlock)response;

/**
 * Calculates heart rate zones based on a user's age
 * @return An array of UAHeartRateZone objects
 */
- (NSArray *)calculateHeartRateZonesWithAge:(NSUInteger)age;

/*
 * Calculates heart rate zones based on a user's max heart rate
 * @return An array of UAHeartRateZone objects
 */
- (NSArray *)calculateHeartRateZonesWithMaxHeartRate:(NSUInteger)maxHeartRate;

@end
