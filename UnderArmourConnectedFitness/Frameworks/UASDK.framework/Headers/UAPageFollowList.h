//
//  UAPageFollowList.h
//  UA
//
//  Created by Corey Roberts on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAEntityList.h>
#import <UASDK/UAPageFollowListRef.h>

@interface UAPageFollowList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UAPageFollowListRef *ref;
/**
 *  A reference to the previous collection of page follows.
 *  @discussion If no more page follows exist, this value will be nil.
 */
@property (nonatomic, strong) UAPageFollowListRef *previousRef;

/**
 *  A reference to the next collection of page follows.
 *  @discussion If no more page follows exist, this value will be nil.
 */
@property (nonatomic, strong) UAPageFollowListRef *nextRef;

@end
