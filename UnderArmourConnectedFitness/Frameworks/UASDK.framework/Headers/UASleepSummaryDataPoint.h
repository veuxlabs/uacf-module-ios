//
//  UASleepSummaryDataPoint.h
//  UASDK
//
//  Created by Jeremiah Smith on 12/9/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UASleepSummaryDataPoint : UADataPoint

/**
 * Sum of the user's sleep time. (double, seconds)
 */
@property (nonatomic, readonly) NSNumber *sleepTimeSum;

/**
 * Avg of the user's sleep time. (double, seconds)
 */
@property (nonatomic, readonly) NSNumber *sleepTimeAvg;

@end
