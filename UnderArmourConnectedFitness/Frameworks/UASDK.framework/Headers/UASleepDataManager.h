/*
 * UASleepDataManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAManager.h>

@class UASleepData, UASleepDataReference, UASleepDataList, UASleepDataListRef;

typedef void (^UASleepDataResponseBlock)(UASleepData *object, NSError *error);
typedef void (^UASleepDataListResponseBlock)(UASleepDataList *object, NSError *error);

@interface UASleepDataManager : UAManager

/**
 *  Saves sleep information
 *
 *  @param sleepData object that represents the data being uploaded for the sleep
 *  @param response A block called on completion.
 */
- (void)createSleepData:(UASleepData *)sleepData
             response:(UASleepDataResponseBlock)response;

- (void)updateSleepData:(UASleepData *)sleepData
			   response:(UASleepDataResponseBlock)response;

- (void)deleteSleepDataWithRef:(UASleepDataReference *)reference
                      response:(UAResponseBlock)response;

/**
 *  Returns a sleep data object for a sleep reference
 *
 *  @param reference reference to the sleep data to fetch
 *  @param response A block called on completion.
 */
- (void)fetchSleepDataWithRef:(UASleepDataReference *)reference
                     response:(UASleepDataResponseBlock)response;

- (void)fetchSleepDataWithListRef:(UASleepDataListRef *)reference
                         response:(UASleepDataListResponseBlock)response;


@end
