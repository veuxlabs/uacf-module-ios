/*
 * UAHeartRateSummaryDerivedDataSourceConfiguration.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UADerivedDataSourceConfiguration.h"

@interface UAHeartRateSummaryDerivedDataSourceConfiguration : UADerivedDataSourceConfiguration <UADataSourceConfigurationInterface>

@end
