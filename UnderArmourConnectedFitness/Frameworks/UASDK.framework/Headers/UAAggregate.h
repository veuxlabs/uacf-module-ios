/*
 * UAAggregate.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@class UADataSeries, UADataTypeRef, UAUserRef;

@interface UAAggregate : NSObject <NSCoding>

/**
 * Summary data of aggregate
 */
@property (nonatomic, strong) UADataSeries *summary;

/**
 * Period data of aggregate
 */
@property (nonatomic, strong) UADataSeries *periods;

/**
 * Duration of each date grouping in the aggregation result.
 */
@property (nonatomic, copy) NSString *period;

/**
 * Link to the data type
 */
@property (nonatomic, strong) UADataTypeRef *dataTypeRef;

/**
 * Link to the user 
 */
@property (nonatomic, strong) UAUserRef *userRef;

/**
 *  Start datetime. May be nil.
 */
@property (nonatomic, strong) NSDate *startDatetime;

/**
 *  End datetime. May be nil.
 */
@property (nonatomic, strong) NSDate *endDatetime;

@end
