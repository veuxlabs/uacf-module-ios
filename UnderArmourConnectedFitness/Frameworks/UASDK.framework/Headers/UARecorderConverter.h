/*
 * UARecorderConverter.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@class UARecorder, UAWorkout, UAActivity;

@interface UARecorderConverter : NSObject

/**
 *  Converts a UARecorder to a UAWorkout.
 *
 *  @param recorder		   The session to be converted.
 *  @param completionBlock A block that contains a converted UAWorkout and an error (if applicable).
 */
- (void)convertRecorder:(UARecorder *)recorder toWorkout:(void (^)(UAWorkout *workout, NSError *error))completionBlock;

/**
 *  Converts a UARecorder to a UAActivity.
 *
 *  @param recorder        The session to be converted.
 *  @param completionBlock A block that contains a converted UAActivity and an error (if applicable).
 * 
 *  @bug This method has not been implemented.
 */
- (void)convertRecorder:(UARecorder *)recorder toActivity:(void (^)(UAActivity *activity, NSError *error))completionBlock;

@end
