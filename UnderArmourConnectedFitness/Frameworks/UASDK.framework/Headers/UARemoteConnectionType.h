/*
 * UARemoteConnectionType.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@class UAUserRef;

@interface UARemoteConnectionTypeRef : UAEntityRef

@end

/**
 *  A type of connection to a remote service.
 */
@interface UARemoteConnectionType : UAEntity <UAEntityInterface>

/**
 *  A reference to this remote connection type.
 */
@property (nonatomic, strong) UARemoteConnectionTypeRef *ref;

/**
 *  An identifier (i.e. unique name) for this type of remote connection.
 */
@property (nonatomic, copy) NSString *recorderTypeKey;

/**
 *  A string to be displayed in the body of an alert view,
 *  when the user decides to disconnect a device.
 */
@property (nonatomic, copy) NSString *disconnectCopyText;

@property (nonatomic, copy) NSString *disconnectCancelText;

@property (nonatomic, copy) NSString *disconnectConfirmText;

/**
 *  The name of the device.
 */
@property (nonatomic, copy) NSString *name;

/**
 *  The type of device.
 */
@property (nonatomic, copy) NSString *type;

/**
 *  The URL for the device's logo (dark on light background).
 */
@property (nonatomic, copy) NSURL *logoURL;

/**
 *  The URL for the device's logo (light on dark background).
 */
@property (nonatomic, copy) NSURL *logoLightURL;


@property (nonatomic, copy) NSURL *oAuthConnectURL;

/**
 *  A string to be displayed in a header view. This string usually
 *  gives instructions on what to do for connecting.
 */
@property (nonatomic, copy) NSString *introCopyHeadingText;

/**
 *  A string to be displayed in the body. This string usually
 *  explains how the connection works and how much data will
 *  be pulled from the device.
 */
@property (nonatomic, copy) NSString *introCopyBodyText;

@end
