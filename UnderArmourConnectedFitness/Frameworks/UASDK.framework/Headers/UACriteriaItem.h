/*
 * UACriteriaItem.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@interface UACriteriaItem : NSObject <NSCoding>

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) id<NSCoding, NSCopying> value;

@end
