/*
 * UAPageListRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityListRef.h>

typedef NS_ENUM(NSUInteger, UAPageListType)
{
	UAPageListTypePublicFigure,
    UAPageListTypePublicEntity,
    UAPageListTypeSuggested,
    UAPageListTypeInitial
};

/**
 *  An object that references a collection of pages.
 */
@interface UAPageListRef : UAEntityListRef

/**
 *  Creates a reference paged collection of pages.
 *
 */
+ (UAPageListRef *)pageListRefForType:(UAPageListType)collectionType;

/**
 *  Creates a reference paged collection of pages with a specified page size.
 *
 */
+ (UAPageListRef *)pageListRefForType:(UAPageListType)collectionType
                                                                    limit:(NSUInteger)limit;

@end
