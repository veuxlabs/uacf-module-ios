//
//  UAActivityStoryTemplate.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UAActivityStoryTemplate : NSObject

/**
 icon url
 */
@property (nonatomic, copy) NSString *icon;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *message;

@end
