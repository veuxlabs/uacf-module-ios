/*
 * UATimeSeriesSpeedPoint.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeriesPoint.h>

@interface UATimeSeriesSpeedPoint : UATimeSeriesPoint

+ (instancetype)pointAtTime:(NSTimeInterval)timestamp withSpeed:(double)speed;

/**
 *  Speed in meters per second
 */
@property (nonatomic, readonly) double speed;

@end
