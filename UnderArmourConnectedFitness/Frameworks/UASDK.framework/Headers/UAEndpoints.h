/*
 * UAEndpoints.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#ifndef UA_UAEndpoints_h
#define UA_UAEndpoints_h

// Users
static NSString * const kOAuth2AccessTokenPath           = @"/v7.0/oauth2/access_token/";
static NSString * const kOAuth2AuthorizePath             = @"/v7.0/oauth2/uacf/authorize/";
static NSString * const kAuthenticatedUserPath           = @"/v7.0/user/self/";
static NSString * const kUserPath                        = @"/v7.0/user/";
static NSString * const kUserProfilePhotoPath            = @"/v7.0/user_profile_photo/";
static NSString * const kUserStatsPath                   = @"/v7.0/user_stats/";
static NSString * const kPasswordResetPath               = @"/api/0.1/password_reset/";
static NSString * const kUserPermissionPath              = @"/api/0.1/user_permission/";
static NSString * const kUserProfilePhotoFormatString    = @"http://drzetlglcbfx.cloudfront.net/profile/%@/picture?size=%@";

// Page
static NSString *const kPagePath                        = @"/api/0.1/page/";
static NSString *const kPageFollowPath                  = @"/api/0.2/page_follow/";
static NSString *const kPageTypePath                    = @"/api/0.1/page_type/";
static NSString *const kPageAssociationPath             = @"/api/0.1/page_association/";

// Gear
static NSString* const kUserGearPath					= @"/api/0.1/usergear/";
static NSString* const kGearPath						= @"/api/0.1/gear/";
static NSString* const kGearBrandPath					= @"/v7.0/gearbrand/";

// Group
static NSString* const kGroupPath                       = @"/v7.0/group/";
static NSString* const kGroupInvitePath                 = @"/api/0.2/group_invite/";
static NSString* const kGroupObjectivePath              = @"/v7.0/group_objective/";
static NSString* const kGroupPurposePath                = @"/v7.0/group_purpose/";
static NSString* const kGroupUserPath                   = @"/v7.0/group_user/";

// Privacy
static NSString * const kPrivacyOptionPath               = @"/v7.0/privacy_option/";

// Social / Friends
static NSString * const kFriendsPath                     = @"/v7.0/user/";
static NSString * const kFriendshipPath                  = @"/api/0.2/friendship/";

// Achievements
static NSString * const kAchievementPath                 = @"/v7.0/achievement/";
static NSString * const kUserAchievementsPath            = @"/v7.0/user_achievement/";

// Workouts
static NSString * const kWorkoutPath                     = @"/v7.0/workout/";

// Activity Type
static NSString * const kActivityTypePath                = @"/v7.0/activity_type/";

// Routes
static NSString * const kRoutePath                       = @"/v7.0/route/";
static NSString * const kRouteBookmarkPath               = @"/v7.0/route_bookmark/";

// Activity Feed
static NSString * const kActivityStoryPath               = @"/api/0.2/activity_story/";
// activity story object privacy update requires 0.1 at the moment
static NSString * const kPrivacyUpdateBasePath           = @"/api/0.1/";

// Moderation
static NSString * const kModerationPath                  = @"/v7.0/moderation_action/";

// Moderation
static NSString * const kModerationActionPath             = @"/v7.0/moderation_action_type/%@/";

// Sleep
static NSString * const kSleepPath                       = @"/api/0.1/sleep/";

// BodyMass
static NSString * const kBodyMassPath                    = @"/api/0.1/bodymass/";

// Activity / Actigraphy
static NSString * const kActigraphyPath                  = @"/api/0.1/actigraphy/";
static NSString * const kActivityTimeseriesPath          = @"/api/0.1/activity_timeseries/";
static NSString * const kActivityPath                    = @"/api/0.1/activity/";

// Aggregate
static NSString * const kAggregatePath					= @"/v7.0/aggregate/";

// Group
static NSString * const kGroupLeaderboardPath			= @"/v7.0/group_leaderboard/";

// Social / Friends
static NSString * const kSuggestedFriendsPath            = @"/api/0.1/friend_suggestion/";

// DataType
static NSString * const kDataTypePath					= @"/v7.0/data_type/";

// Remote Connection
static NSString *const kDevicesPath                     = @"/api/0.1/remoteconnectiontype/";
static NSString *const kUserDevicesPath                 = @"/api/0.1/remoteconnection/";
static NSString *const kDevicePreferencesPath           = @"/api/0.1/actigraphy_settings/";
static NSString *const kSaveDevicePreferencesPath       = @"/api/0.1/actigraphy_recorder_priority/";

// Heart Rate Zones
static NSString *const kHeartRateZonesPath				= @"/v7.0/heart_rate_zones/";
static NSString *const kHeartRateZoneCalculationPath	= @"/v7.0/heart_rate_zone_calculation/";

// Image Path
static NSString* const kImagePath                       = @"/api/0.1/image/";

// File Mobile (video upload)
static NSString* const kFileMobileTokenPath             = @"/api/0.1/filemobile_session/";
static NSString* const kFileMobileBaseUrl               = @"http://api.filemobile.com/";
static NSString* const kFileMobileUploadPath            = @"/services/upload2?json";

#endif
