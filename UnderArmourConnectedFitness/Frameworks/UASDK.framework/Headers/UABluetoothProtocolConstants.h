/*
 * UABluetoothProtocolConstants.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#ifndef UASDK_UABluetoothProtocolConstants_h
#define UASDK_UABluetoothProtocolConstants_h

typedef NS_ENUM(NSUInteger, UABluetoothHeartRateMonitorSensorContactStatus) {
	UABluetoothHeartRateMonitorSensorContactStatusUnsupported,
	UABluetoothHeartRateMonitorSensorContactStatusNoContactDetected,
	UABluetoothHeartRateMonitorSensorContactStatusContactDetected,
};

#endif
