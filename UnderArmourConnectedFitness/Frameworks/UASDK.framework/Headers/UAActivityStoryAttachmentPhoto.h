//
//  UAActivityStoryAttachmentPhoto.h
//  UA
//
//  Created by Jeff Oliver on 7/11/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import "UAActivityStoryAttachment.h"

@interface UAActivityStoryAttachmentPhoto : UAActivityStoryAttachment

@property (nonatomic, strong) NSString *template;

@end
