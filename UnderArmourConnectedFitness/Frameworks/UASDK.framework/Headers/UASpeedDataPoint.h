//
//  UASpeedDataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/20/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UASpeedDataPoint : UADataPoint

/**
 *  Speed (double, meters/second).
 */
@property (nonatomic, readonly) NSNumber *speed;

/**
 *  Instantiates a speed data point.
 *  @param speed The speed in meters/second. Must not be nil.
 *  @return A newly instantiated data point.
 */
- (instancetype)initWithSpeed:(NSNumber *)speed NS_DESIGNATED_INITIALIZER;

@end
