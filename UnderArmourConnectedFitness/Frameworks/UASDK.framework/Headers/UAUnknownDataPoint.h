//
//  UAUnknownDataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/19/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UAUnknownDataPoint : UADataPoint

@end
