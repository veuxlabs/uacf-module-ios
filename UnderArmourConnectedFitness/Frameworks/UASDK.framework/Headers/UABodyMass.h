/*
 * UABodyMass.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>
#import <UASDK/UATimeSeries.h>

extern NSString * const kUAManualBodyMassCreationRecorderType;

@class UAUserRef;

@interface UABodyMassReference : UAEntityRef

@end

@interface UABodyMass : UAEntity <UAEntityInterface, NSCoding>

/**
 *  The reference to this object
 */
@property (nonatomic, strong) UABodyMassReference *ref;

/**
 *  The reference of the user associated with this object
 */
@property (nonatomic, strong) UAUserRef *userRef;

/**
 * The date this data is valid
 */
@property (nonatomic, strong) NSDate *dateTime;

/**
 * The date this data was created at.
 */
@property (nonatomic, strong) NSDate *createdDateTime;

/**
 *  The date this data was updated.
 */
@property (nonatomic, strong) NSDate *updatedDateTime;

/**
 * The timezone at which this data was recorded from.
 */
@property (nonatomic, strong) NSTimeZone *timezone;

/**
 * A string that represents what kind of device was used to record this data
 */
@property (nonatomic, copy) NSString *referenceKey;

/**
 *  A string that represents a unique identifier for this recording object. This is generated automatically when saving.
 *  @discussion When manually logging body mass, set to kUAManualBodyMassCreationRecorderType
 */
@property (nonatomic, copy) NSString *recorderTypeKey;

/**
 *  The user's body mass index (bmi). See http://en.wikipedia.org/wiki/Body_mass_index
 */
@property (nonatomic, strong) NSNumber *bmiNumber;
@property (nonatomic, assign) double bmi;

/**
 *  The user's total mass in kg. If lean_mass and fat_mass are present, they should add up to this field.
 */
@property (nonatomic, strong) NSNumber *massNumber;
@property (nonatomic, assign) double mass;

/**
 *  The user's total mass (in kg) that is not fat.
 */
@property (nonatomic, strong) NSNumber *leanMassNumber;
@property (nonatomic, assign) double leanMass;

/**
 *  The user's total mass (in kg) that is fat.
 */
@property (nonatomic, strong) NSNumber *fatMassNumber;
@property (nonatomic, assign) double fatMass;

/**
 *  The percent of user's body mass that consists of fat. For example, "15.738" would mean 15.738% of user's total
 */
@property (nonatomic, strong) NSNumber *fatPercentNumber;
@property (nonatomic, assign) double fatPercent;

@end


