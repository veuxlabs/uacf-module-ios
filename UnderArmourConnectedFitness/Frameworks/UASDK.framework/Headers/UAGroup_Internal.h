/*
 * UAGroup_Internal.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#ifndef UASDK_UAGroup_Internal_h
#define UASDK_UAGroup_Internal_h

#import "UAGroup.h"

@interface UAGroup ()

@property (nonatomic, readwrite) NSNumber *maxUsers;

@end

#endif
