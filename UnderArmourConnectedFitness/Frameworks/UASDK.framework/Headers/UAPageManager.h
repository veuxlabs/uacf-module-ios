/*
 * UAPageManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAManager.h>

@class UAPageRef, UAUserRef, UAPage, UAPageType, UAPageTypeRef, UAPageTypeList,
       UAPageAssociationList, UAPageAssociationRef, UAPageFollowRef, UAPageFollowList,
       UAPageFollow, UAPageList, UAPageListRef, UAPageAssociationListRef,
       UAPageFollowListRef;

typedef void (^UAPageListResponseBlock)(UAPageList *object, NSError *error);
typedef void (^UAPageResponseBlock)(UAPage *object, NSError *error);
typedef void (^UAPageTypeResponseBlock)(UAPageType *object, NSError *error);
typedef void (^UAPageFollowResponseBlock)(UAPageFollow *object, NSError *error);
typedef void (^UAPageFollowListResponse)(UAPageFollowList *object, NSError *error);
typedef void (^UAPageTypeListResponseBlock)(UAPageTypeList *object, NSError *error);
typedef void (^UAPageAssociationsResponseBlock)(UAPageAssociationList *object, NSError *error);

/**
 *  UAPageManager is a management class that handles UAPage entities.
 */
@interface UAPageManager : UAManager

/**
 *  NOTE: This is still in development on the server and hasn't been verified. Do not use at the moment.
 *
 *  Gets a paged collection of pages
 *
 *  @param reference A reference to a page collection of pages.
 *  @param response A block called on completion.
 */
- (void)fetchPagesWithListRef:(UAPageListRef *)reference
                     response:(UAPageListResponseBlock)response;
/**
 *  Gets a user page with a given reference.
 *
 *  @param reference A reference to a Page.
 *  @param response A block called on completion.
 */
- (void)fetchPageWithRef:(UAPageRef *)reference
                response:(UAPageResponseBlock)response;


/**
 *  Returns a list of available page types.
 *
 *  @param response A block called on completion.
 */
- (void)fetchPageTypes:(UAPageTypeListResponseBlock)response;


/**
 *  Gets an UAPageType for a reference.
 *
 *  @param reference A reference to a page type.
 *  @param response A block called on completion.
 */
- (void)fetchPageTypeWithRef:(UAPageTypeRef *)reference
                    response:(UAPageTypeResponseBlock)response;

/**
 *  Gets an reference for a given UAPageAssociationListRef
 *
 *  @param reference A page association collection reference.
 *  @param response A block called on completion.
 */
- (void)fetchPageAssociationsWithListRef:(UAPageAssociationListRef *)reference
                                response:(UAPageAssociationsResponseBlock)response;


/**
 *  Gets a list of UAPageFollowRef objects based on a user reference.
 *
 *  @param reference A user reference.
 *  @param response A block called on completion.
 *
 *	@discussion This method gets a list of page follow objects that the provided user reference
 *	follows
 */
- (void)fetchPageFollowsForUserWithRef:(UAUserRef *)reference
                              response:(UAPageFollowListResponse)response;


/**
 *  Gets a list of UAPageFollowRef objects based on a page reference.
 *
 *  @param reference A page reference.
 *  @param response A block called on completion.
 *
 *	@discussion This method gets a list of page follow objects that represent a page's
 *	followers
 */
- (void)fetchPageFollowsWithRef:(UAPageRef *)reference
                       response:(UAPageFollowListResponse)response;


/**
 *  Gets an UAPageFollowList for a given UAPageFollowListRef.
 *
 *  @param reference A page follow collection reference.
 *  @param response A block called on completion.
 *
 *	@discussion This method gets a list of page follow objects that represent a page's
 *	followers
 */
- (void)fetchPageFollowsWithListRef:(UAPageFollowListRef *)reference
                           response:(UAPageFollowListResponse)response;

/**
 *  Determines if a user is following a page.
 *
 *  @param userRef A user reference.
 *  @param pageRef A page reference.
 *  @param response A block called on completion.
 */
- (void)fetchFollowStatusWithUserRef:(UAUserRef *)userRef
                      forPageWithRef:(UAPageRef *)pageRef
                            response:(UAPageFollowListResponse)response;

/**
 *  Posts a request for a user to follow a page.
 *
 *  @param pageFollow A page follow object that has a user reference 
 *		and page reference that will be followed.
 *  @param response A block called on completion.
 */
- (void)createPageFollow:(UAPageFollow *)pageFollow
				response:(UAPageFollowResponseBlock)response;

/**
 *  Posts a request for a user to follow an array of pages.
 *
 *  @param pageFollows An array of page follow objects
 *  @param response A block called on completion.
 */
- (void)createPageFollows:(NSArray *)pageFollows
				 response:(UAPageFollowListResponse)response;
/**
 *  The page will be unfollowed by the user.
 *
 *  @param reference A page reference that will be unfollowed.
 *  @param response A block called on completion.
 */
- (void)deleteFollowWithRef:(UAPageFollowRef *)reference
                   response:(UAResponseBlock)response;

@end
