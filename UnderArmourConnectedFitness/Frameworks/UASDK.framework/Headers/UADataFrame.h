/*
 * UADataFrame.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class UADataPoint, UADataTypeRef;
@class UADistanceDataPoint, UALocationDataPoint, UAElevationDataPoint, UAEnergyExpendedDataPoint, UASpeedDataPoint, UASpeedSummaryDataPoint, UAWillpowerDataPoint, UAHeartRateDataPoint, UAHeartRateSummaryDataPoint, UAIntensityDataPoint, UAStepsDataPoint;

@interface UADataFrame : NSObject

/**
 *  Grabs a data point (if it exists) based on the data type.
 *
 *  @param dataTypeRef A UADataTypeRef.
 *
 *  @return A data point that represents this data type (if it exists).
 */
- (UADataPoint *)dataPointWithDataTypeRef:(UADataTypeRef *)dataTypeRef;

@property (nonatomic, readonly) BOOL started;

@property (nonatomic, readonly) double elapsedTime;

@property (nonatomic, readonly) double activeTime;

@property (nonatomic, readonly) UADistanceDataPoint *distanceDataPoint;

@property (nonatomic, readonly) UALocationDataPoint *locationDataPoint;

@property (nonatomic, readonly) UAElevationDataPoint *elevationDataPoint;

@property (nonatomic, readonly) UAEnergyExpendedDataPoint *energyExpendedDataPoint;

@property (nonatomic, readonly) UASpeedDataPoint *speedDataPoint;

@property (nonatomic, readonly) UASpeedSummaryDataPoint *speedSummaryDataPoint;

@property (nonatomic, readonly) UAWillpowerDataPoint *willpowerDataPoint;

@property (nonatomic, readonly) UAHeartRateDataPoint *heartRateDataPoint;

@property (nonatomic, readonly) UAHeartRateSummaryDataPoint *heartRateSummaryDataPoint;

@property (nonatomic, readonly) UAIntensityDataPoint *intensityDataPoint;

@property (nonatomic, readonly) UAStepsDataPoint *stepsDataPoint;

@end
