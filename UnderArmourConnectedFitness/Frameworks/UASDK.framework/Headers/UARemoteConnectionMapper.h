/*
 * UARemoteConnectionMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

@import Foundation;

@class UARemoteConnection, UARemoteConnectionList;

@interface UARemoteConnectionMapper : NSObject

+ (UARemoteConnection *)remoteConnectionFromDictionary:(NSDictionary *)dictionary;

+ (UARemoteConnectionList *)remoteConnectionListFromDictionary:(NSDictionary *)dictionary;

@end
