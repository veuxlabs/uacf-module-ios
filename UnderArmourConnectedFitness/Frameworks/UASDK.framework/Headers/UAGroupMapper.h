/*
 * UAGroupMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@class UAGroup;
@class UAGroupRef;
@class UAGroupList;

@interface UAGroupMapper : NSObject

+ (UAGroupRef *)groupRefFromDictionary:(NSDictionary *)response;

+ (UAGroup *)groupFromDictionary:(NSDictionary *)response;

+ (UAGroupList *)groupListFromDictionary:(NSDictionary *)response;

+ (NSDictionary *)dictionaryFromGroup:(UAGroup *)group;

@end
