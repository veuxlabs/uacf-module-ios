/*
 * UABrandMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@interface UABrandMapper : NSObject

// TODO:  - naming convention inconsistent with other mappers.
+ (NSArray *)brandsFromArray:(NSArray *)brands;

@end
