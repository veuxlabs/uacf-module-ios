/*
 * UAEntityRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAValidationProtocol.h>

/**
 *  An object that references an entity.
 */
@interface UAEntityRef : NSObject <NSCoding, UAValidationProtocol>

/**
 *  An identification value for this object.
 */
@property (nonatomic, copy, readonly) NSString *entityID;


- (BOOL)isEqualToEntityRef:(UAEntityRef *)ref;

@end
