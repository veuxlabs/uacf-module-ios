/*
 * UABluetoothDataSource.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UASensorDataSource.h"
#import "UABluetoothServiceTypes.h"

@class UABluetoothClient;

@interface UABluetoothDataSource : UASensorDataSource

/**
 *  Instantiates a new Bluetooth message producer.
 *
 *  @param deviceUUID The UUID of the device which this producer will connect to. Acquired from a CBPeripheral object.
 *  @param services The services supported by the device for which this producer will produce messages. More than one service can be combined using a bitwise-OR.
 *
 *  @discussion The device UUID and list of services provided to this method will be used to create a {@link UABluetoothClient} which will maintain a connection
 *  to the device and produce messages based on its readings once {@link begin} has been called.
 */
- (instancetype)initWithDeviceUUID:(NSUUID *)deviceUUID services:(UABluetoothServiceType)services;

/**
 *  Instantiates a new Bluetooth message producer.
 *
 *  @param client A {@link UABluetoothClient} instance configured for the desired device ID and services.
 *  @see initWithDeviceUUID:services:
 */
- (instancetype)initWithBluetoothClient:(UABluetoothClient *)client;

@end
