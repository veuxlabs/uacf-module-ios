/*
 * UAActivityRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntityRef.h>

/**
 *  An object that references an activity.
 */
@interface UAActivityRef : UAEntityRef

@end
