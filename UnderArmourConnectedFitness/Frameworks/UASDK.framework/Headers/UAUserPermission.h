/*
 * UAUserPermission.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

@import Foundation;

#import <UASDK/UAEnumerations.h>

typedef NSInteger UAUserPermissionTypeBitmask;

@class UAEntityRef;

@interface UAUserPermission : NSObject

@property (nonatomic, strong) UAEntityRef *ref;
@property (nonatomic, assign) UAUserPermissionTypeBitmask permissionTypes;

@end
