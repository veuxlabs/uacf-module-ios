//
//  UAActivityStoryComment.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import "UAActivityStory.h"
#import "UAActivityStoryActor.h"

@interface UAActivityStoryComment : UAActivityStory

@property (nonatomic, copy) NSString *commentID;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, strong) UAActivityStoryActor *actor;
@property (nonatomic, strong) NSDate *published;

@end
