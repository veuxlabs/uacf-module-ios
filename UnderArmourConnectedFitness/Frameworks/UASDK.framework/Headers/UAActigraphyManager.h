/*
 * UAActigraphyManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAManager.h>

@class UAActigraphyListRef, UAActigraphyList;

typedef void (^UAActigaphyListResponseBlock)(UAActigraphyList *object, NSError *error);

@interface UAActigraphyManager : UAManager

/**
 *  Fetches the actigraphy data for a actigraphy paged collection reference
 *
 *  @param reference the actigraphy paged collection reference
 *  @param response A block called on completion.
 */
- (void)fetchActigraphyWithListRef:(UAActigraphyListRef *)reference
                           response:(UAActigaphyListResponseBlock)response;

- (void)fetchActigraphyWithListRef:(UAActigraphyListRef *)reference
            withCachePolicy:(UAObjectCachePolicy)cachePolicy
                   response:(UAActigaphyListResponseBlock)response;

@end
