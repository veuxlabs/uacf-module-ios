/*
 * UARouteMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


@import Foundation;

@class UARoute, UARouteFilter, UARouteBookmarkFilter, UARouteList, UARouteBookmarkList, UARouteBookmark;

/**
 *  A mapper class that can map a dictionary containing appropriate keys to
 *  an UARouteMapper and vice-versa.
 */
@interface UARouteMapper : NSObject

/**
 *  Takes a response from a route web service and converts it to an
 *  UARoute.
 *
 *  @param response a response from a route web service.
 *
 *  @return an UARoute object from the response.
 */
+ (UARoute *)UARouteFromResponse:(NSDictionary *)response;

/**
 *  Creates a route collection based on a routes response.
 *
 *  @param response a response from a routes web service.
 *
 *  @return an UARouteList with a list of routes and links to other collections.
 */
+ (UARouteList *)routeListFromResponse:(NSDictionary *)response;

/**
 *  Creates a route bookmark collection based on a routes response.
 *
 *  @param response a response from a routes bookmark web service.
 *
 *  @return an UARouteBookmarkList with a list of UARouteBookmark objects
 *  and links to other collections.
 */
+ (UARouteBookmarkList *)bookmarkListFromResponse:(NSDictionary *)response;

/**
 *  Takes a response from a routes web service and converts the data
 *  into an array of UARoutes.
 *
 *  @param response a response from a routes web service.
 *
 *  @return an array of UARoutes from the response.
 */
+ (NSArray *)UARoutesFromResponse:(NSDictionary *)response;

/**
 *  Takes a response from a bookmarked routes web service and converts the
 *  data into an array of UARouteBookmark objects.
 *
 *  @param response a response from a bookmarked routes web service.
 *
 *  @return an array of UARouteBookmark objects from the response.
 */
+ (NSArray *)UARoutesFromBookmarkResponse:(NSDictionary *)response;

/**
 *  Creates a dictionary representation of an UARoute, to be primarily
 *  used in web services.
 *
 *  @param route an UARoute to convert.
 *
 *  @return an NSDictionary representation of an UARoute.
 */
+ (NSDictionary *)dictionaryFromRoute:(UARoute *)route;

/**
 *  Updates an UARoute based on a response given back by creating a route.
 *
 *  @param route    an UARoute object to update.
 *  @param response a response from adding a new route.
 *
 *  @return an updated UARoute.
 */
+ (UARoute *)updateRoute:(UARoute *)route withResponse:(NSDictionary *)response;

+ (UARouteBookmark *)routeBookmarkSummaryFromResponse:(NSDictionary *)response;

#pragma mark - Other Methods

+ (void)setRouteElevations:(UARoute *)route withArray:(NSArray *)array;

+ (UARoute *)dictionaryToRouteClimbStats:(UARoute *)route dict:(NSDictionary *)result;

@end
