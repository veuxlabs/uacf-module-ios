/*
 * UARecorderContext.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@class UAUser, UAActivityType, UAHeartRateZones, UAProcessorMessageQueue, UARecordClock;

@interface UARecorderContext : NSObject

/**
 *  The name of the context.
 */
@property (nonatomic, copy, readonly) NSString *name;

/**
 *  The user associated with this context.
 */
@property (nonatomic, readonly) UAUser *user;

/**
 *  The activity type associated with this context. Can be changed.
 */
@property (nonatomic, strong) UAActivityType *activityType;

/**
 *  The user's configured heart rate zones. Can be changed.
 */
@property (nonatomic, strong) UAHeartRateZones *heartRateZones;

/**
 *  The processor message queue associated with this context.
 */
@property (nonatomic, weak, readonly) UAProcessorMessageQueue *processorMessageQueue;

/**
 *  A clock designating the current running time on the device.
 */
@property (nonatomic, readonly) UARecordClock *recordClock;

/**
 *  Creates a new recorder context.
 *
 *  @param name                  The name of the context.
 *  @param user                  The user associated with this context.
 *  @param activityType          The activity type associated with this context. Can be changed.
 *  @param heartRateZones		 The user's configured heart rate zones. Can be changed.
 *  @param processorMessageQueue The processor message queue associated with this context.
 *  @param recordClock		     The record clock.
 *
 *  @return A new recorder context.
 */
- (instancetype)initWithName:(NSString *)name user:(UAUser *)user activityType:(UAActivityType *)activityType heartRateZones:(UAHeartRateZones *)heartRateZones processorMessageQueue:(UAProcessorMessageQueue *)processorMessageQueue recordClock:(UARecordClock *)recordClock NS_DESIGNATED_INITIALIZER;

@end
