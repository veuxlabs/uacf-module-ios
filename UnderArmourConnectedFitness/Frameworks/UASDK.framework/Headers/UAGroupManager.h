/*
 * UAGroupManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>
#import <UASDK/UAManager.h>

@class UAGroup;
@class UAGroupList;
@class UAGroupRef;
@class UAGroupListRef;

typedef void (^UAGroupResponseBlock)(UAGroup *object, NSError *error);
typedef void (^UAGroupListResponseBlock)(UAGroupList *object, NSError *error);

@interface UAGroupManager : UAManager

- (void)fetchGroupWithGroupRef:(UAGroupRef *)reference response:(UAGroupResponseBlock)response;

- (void)fetchGroupsWithListRef:(UAGroupListRef *)reference response:(UAGroupListResponseBlock)response;

- (void)createGroup:(UAGroup *)group response:(UAGroupResponseBlock)response;

- (void)updateGroup:(UAGroup *)group resposne:(UAGroupResponseBlock)response;

- (void)deleteGroup:(UAGroupRef *)reference response:(UAResponseBlock)response;

@end
