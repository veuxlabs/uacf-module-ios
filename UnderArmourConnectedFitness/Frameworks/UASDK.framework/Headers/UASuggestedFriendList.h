/*
 * UASuggestedFriendList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@class UAUserRef;

@interface UASuggestedFriendListRef : UAEntityListRef

+ (UASuggestedFriendListRef *)getSuggestedFriendsListRefForUser:(UAUserRef *)userRef;

+ (UASuggestedFriendListRef *)getSuggestedFriendsListRefForUser:(UAUserRef *)userRef
                                                      withLimit:(NSUInteger)limit;

/**
 * Gets a list of suggested friends references for a particular source
 * @param userRef The user for which the API will suggest friends.
 * @param source is the source of the desired suggested friends. You can see available see UAResourceKeys 
 */

+ (UASuggestedFriendListRef *)getSuggestedFriendsListRefForUser:(UAUserRef *)userRef
                                                     withSource:(NSString *)source;

@end

@interface UASuggestedFriendList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UASuggestedFriendListRef *ref;

/**
 *  A reference to the previous collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UASuggestedFriendListRef *previousRef;

/**
 *  A reference to the next collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UASuggestedFriendListRef *nextRef;


@end
