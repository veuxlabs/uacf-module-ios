//
//  UAEnergyExpendedSummaryDataPoint.h
//  UASDK
//
//  Created by mallarke on 11/27/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UAEnergyExpendedSummaryDataPoint : UADataPoint

/**
 *  Energy expended summary (double, joules).
 */
@property (nonatomic, readonly) NSNumber *energyExpendedSum;

@end
