/*
 * UADerivedDataSourceConfiguration.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UADataSourceConfiguration.h"

@interface UADerivedDataSourceConfiguration : UADataSourceConfiguration <UADataSourceConfigurationInterface>

@end
