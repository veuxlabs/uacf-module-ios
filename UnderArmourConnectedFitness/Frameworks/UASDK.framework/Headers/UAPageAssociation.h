/*
 * UAPageAssociation.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntity.h>
#import <UASDK/UAPageAssociationRef.h>
#import <UASDK/UAPageRef.h>

/**
 *  An object that represents the associations of pages for a given type.
 */
@interface UAPageAssociation : UAEntity <UAEntityInterface>

@property (nonatomic, strong) UAPageAssociationRef *ref;

@property (nonatomic, strong) UAPageRef *fromPage;

@property (nonatomic, strong) UAPageRef *toPage;

@end
